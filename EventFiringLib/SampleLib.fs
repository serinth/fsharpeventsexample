﻿namespace EventFiringLib

open System
open System.Threading

module FileManager =

    //This is the custom EventArgs that the calling C# program wants to use and
    //acquire additional information about the file task. In this case we want the output filename after
    //task completion
    type FileTaskEventArgs(filename : string) =
        inherit System.EventArgs()
        member this.Filename = filename


    type FileTask() = 
        
        let event = new DelegateEvent<EventHandler<FileTaskEventArgs>>()
        
        [<CLIEvent>]
        member this.TaskCompletedEvent = event.Publish

        member this.ExecuteTask = 
            //do some long running task
            Thread.Sleep(5000);

            //trigger the event
            event.Trigger([| this;new FileTaskEventArgs(DateTime.Now.ToString("yyyy-MM-dd")+".csv") |])
            ignore
