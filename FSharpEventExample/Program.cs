﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventFiringLib;

namespace FSharpEventExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileManager = new FileManager.FileTask();
            fileManager.TaskCompletedEvent += new EventHandler<FileManager.FileTaskEventArgs>(OnFinishedFileTask);

            Task doFileTask = new Task(() =>
                {
                    Console.WriteLine("Executing File Task...");
                    var result = fileManager.ExecuteTask;
                }
            );
            doFileTask.Start();

            Console.WriteLine("Continuing Execution...");

            Console.ReadLine();

        }

        private static void OnFinishedFileTask(object sender, FileManager.FileTaskEventArgs e)
        {
            Console.WriteLine("Completed File Task with output file: " + e.Filename);
        }
    }
}
